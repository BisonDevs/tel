import 'package:logger/logger.dart';

class Result<T> {
  T result;
  String errorMessage;
  Logger logger = new Logger();

  Result.success();

  Result(this.result);

  Result.error(String message) {
    errorMessage = message;
    logger.e(errorMessage);
  }

  Result.voidError() {
    errorMessage = "Error without specified message";
    logger.e(this.errorMessage);
  }

  Result.fromError(dynamic error) {
    this.errorMessage = error.toString();
    logger.e(this.errorMessage, null, error.stackTrace);
  }

  Result.fromResult(Result result) {
    this.errorMessage = result.errorMessage;
  }

  bool isSuccess() {
    return errorMessage == null;
  }

  bool isError() {
    return errorMessage != null;
  }

  @override
  String toString() {
    return 'Result{result: $result, error: $errorMessage';
  }
}
