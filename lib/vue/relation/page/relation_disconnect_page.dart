import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:ressource_relationnel/main.i18n.dart';

class RelationDisconnectPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: AutoSizeText("Vous n'êtes pas connecté".i18n),);
  }
}
