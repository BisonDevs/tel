// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'relation_form_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RelationFormController on _RelationFormController, Store {
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_RelationFormController.isLoading'))
          .value;

  final _$errorAtom = Atom(name: '_RelationFormController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$messageAtom = Atom(name: '_RelationFormController.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$errorMessageAtom = Atom(name: '_RelationFormController.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$requestFutureAtom =
      Atom(name: '_RelationFormController.requestFuture');

  @override
  ObservableFuture<dynamic> get requestFuture {
    _$requestFutureAtom.reportRead();
    return super.requestFuture;
  }

  @override
  set requestFuture(ObservableFuture<dynamic> value) {
    _$requestFutureAtom.reportWrite(value, super.requestFuture, () {
      super.requestFuture = value;
    });
  }

  final _$ajouterRelationAsyncAction =
      AsyncAction('_RelationFormController.ajouterRelation');

  @override
  Future<bool> ajouterRelation(
      {User targetUser, User currentUser, TypeRelation typeRelation}) {
    return _$ajouterRelationAsyncAction.run(() => super.ajouterRelation(
        targetUser: targetUser,
        currentUser: currentUser,
        typeRelation: typeRelation));
  }

  @override
  String toString() {
    return '''
error: ${error},
message: ${message},
errorMessage: ${errorMessage},
requestFuture: ${requestFuture},
isLoading: ${isLoading}
    ''';
  }
}
