import 'package:ressource_relationnel/modele/categorie_ressource/categorie_ressource.dart';
import 'package:ressource_relationnel/modele/ressource/ressource.dart';
import 'package:ressource_relationnel/modele/type_ressource/type_ressource.dart';
import 'package:ressource_relationnel/modele/result.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/modele_api/ressource_api/ressource_api.dart';

import 'data_source_ressource.dart';

abstract class RepositoryRessourceAbstract {
  Future<Result<List<TypeRessource>>> getListTypeRessource();

  Future<Result<List<CategorieRessource>>> getListCategorieRessource();

  Future<Result<List<Ressource>>> getListRessourceDeUser(
      User connectedUser, User targetUser);

  Future<Result<List<Ressource>>> getListRessource();

  Future<Result<List<Ressource>>> getListRessourceConnect(String token);

  Future<Result<List<Ressource>>> getListRessourceFavoris(String token);

  Future<Result<bool>> ajouterRessource({RessourceApi ressourceApi});
}

class RepositoryRessource extends RepositoryRessourceAbstract {
  DataSourceRessource dataRessource = new DataSourceRessource();
  String idTypeRessToCreate;
  String idCategorieRessToCreate;

  Future<Result<List<TypeRessource>>> getListTypeRessource() async {
    var listTypeRess = await dataRessource.getListTypeRessource();
    if (listTypeRess.isSuccess()) {
      return Result(listTypeRess.result);
    } else {
      return Result.error(listTypeRess.errorMessage);
    }
  }

  Future<Result<List<CategorieRessource>>> getListCategorieRessource() async {
    var listCatRess = await dataRessource.getListCategorieRessource();
    if (listCatRess.isSuccess()) {
      return Result(listCatRess.result);
    } else {
      return Result.error(listCatRess.errorMessage);
    }
  }

  Future<Result<List<Ressource>>> getListRessourceDeUser(
      User connectedUser, User targetUser) async {
    if (targetUser == null) {
      targetUser = connectedUser;
    }
    String token = null;
    String uuid = null;
    if (targetUser != null) {
      token = connectedUser.token;
    }
    if (targetUser != null) {
      uuid = targetUser.uuid;
    }
    var listRessUser = await dataRessource.getListRessourceDeUser(token, uuid);
    if (listRessUser.isSuccess()) {
      return Result(listRessUser.result);
    } else {
      return Result.error(listRessUser.errorMessage);
    }
  }

  Future<Result<List<Ressource>>> getListRessource() async {
    var listRess = await dataRessource.getListRessource();
    if (listRess.isSuccess()) {
      return Result(listRess.result);
    } else {
      return Result.error(listRess.errorMessage);
    }
  }

  Future<Result<List<Ressource>>> getListRessourceConnect(String token) async {
    var listRess = await dataRessource.getListRessourceConnect(token);
    if (listRess.isSuccess()) {
      return Result(listRess.result);
    } else {
      return Result.error(listRess.errorMessage);
    }
  }

  Future<Result<List<Ressource>>> getListRessourceFavoris(String token) async {
    var listRess = await dataRessource.getListRessourceFavoris(token);
    if (listRess.isSuccess()) {
      return Result(listRess.result);
    } else {
      return Result.error(listRess.errorMessage);
    }
  }

  @override
  Future<Result<bool>> ajouterRessource({RessourceApi ressourceApi}) async {
    var requestFuture = await dataRessource.ajouterRessource(ressourceApi);
    if (requestFuture.isSuccess()) {
      return Result(requestFuture.result);
    } else {
      return Result.error(requestFuture.errorMessage);
    }
  }
}
