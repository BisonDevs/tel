import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:ressource_relationnel/modele/relation/relation.dart';
import 'package:ressource_relationnel/modele/result.dart';
import 'package:ressource_relationnel/modele/type_relation/type_relation.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/main.i18n.dart';
import 'package:ressource_relationnel/modele_api/etat_relation_api/etat_relation_api.dart';
import 'package:ressource_relationnel/modele_api/relation_api/relation_api.dart';

import '../../stock_const.dart';

class DataSourceRelation {
  Future<Result<List<Relation>>> getListRelation(User user) async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      dio.options.headers["authorization"] = "Bearer ${user.token}";

      print("${url_api}users/${user.uuid}/relations");

      Response response =
          await dio.get("${url_api}users/${user.uuid}/relations");

      var jsonResponse = jsonDecode(response.data);
      var listRelation = jsonResponse["hydra:member"]
          .map((e) => Relation.fromJson(e))
          .cast<Relation>()
          .toList();

      return Result(listRelation);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  Future<Result<List<TypeRelation>>> getListTypeRelation() async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";

      Response response = await dio.get("${url_api}type_relations");

      var jsonResponse = jsonDecode(response.data);
      var listTypeRelation = jsonResponse["hydra:member"]
          .map((e) => TypeRelation.fromJson(e))
          .cast<TypeRelation>()
          .toList();

      return Result(listTypeRelation);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  Future<Result<bool>> ajouterRelation(
      {User currentUser, User targetUser, TypeRelation typeRelation}) async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      dio.options.headers["authorization"] = "Bearer ${currentUser.token}";
      RelationApi relationApi = new RelationApi(
          demandeur: currentUser.id,
          receveur: targetUser.id,
          typeRelation: typeRelation.id);
      print(currentUser.id);
      print(targetUser.id);
      print(typeRelation.id);
      print(relationApi.toJson());
      await dio.post("${url_api}relations", data: relationApi.toJson());

      return Result(true);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
      if (e.response.statusCode == 500) {
        return Result.error("Vous avez déjà fait cette demande d'amis.".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  Future<Result<List<Relation>>> getListMesDemandes({User currentUser}) async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      dio.options.headers["authorization"] = "Bearer ${currentUser.token}";
      print(currentUser.token);
      print("${url_api}users/${currentUser.uuid}/demandes_en_attente");
      Response response = await dio
          .get("${url_api}users/${currentUser.uuid}/demandes_en_attente");
      print("Reponse data : ${response.data}");
      var jsonResponse = jsonDecode(response.data);
      var listDemandes = jsonResponse["hydra:member"]
          .map((e) => Relation.fromJson(e))
          .cast<Relation>()
          .toList();

      return Result(listDemandes);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  Future<Result<bool>> effacerRelation(
      {Relation relation, String token}) async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      dio.options.headers["authorization"] = "Bearer ${token}";

      await dio.delete("${url}${relation.id}");
      return Result(true);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  Future<Result<List<Relation>>> getListDemandesEnAttente(
      {User currentUser}) async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      dio.options.headers["authorization"] = "Bearer ${currentUser.token}";

      Response response = await dio
          .get("${url_api}users/${currentUser.uuid}/receptions_en_attente");
      print("Reponse data : ${response.data}");
      var jsonResponse = jsonDecode(response.data);
      var listDemandes = jsonResponse["hydra:member"]
          .map((e) => Relation.fromJson(e))
          .cast<Relation>()
          .toList();

      return Result(listDemandes);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      print("Mon pb list des dmande en attente : $e");
      return Result.error("Il y a un problème !");
    }
  }

  Future<Result<bool>> decisionRelation(
      {Relation relation, String token, EtatRelationApi etat}) async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      dio.options.headers["authorization"] = "Bearer ${token}";
      await dio.post("${url}${relation.id}/active?active=$etat",
          data: etat.toJson());
      return Result(true);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }
}
