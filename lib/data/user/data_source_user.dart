import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:ressource_relationnel/modele/result.dart';
import 'dart:io';
import 'package:ressource_relationnel/stock_const.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/main.i18n.dart';

/**
 *   "email": "rom@gmail.com",
    "roles": [
    "string"
    ],
    "nom": "rom",
    "prenom": "tom",
    "password": "aAcvbWXNbB12#@"
 */
class DataSourceUser {
  Future<Result<User>> authentification(String email, String password) async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] =
          "application/json"; // C'est quoi ici le type dconnrdtu m'avais j'ai la solution
      User user = new User(email: email, password: password);
      print("url authentification : ${url_api}login_check");
      Response response =
          await dio.post("${url_api}login_check", data: user.toJson(user));
      user = User.fromJson(response.data["user"]);
      user.token = response.data["token"];
      String id = "/api/users/${user.uuid}";
      user.id = id;
      print("token : ${user.token}");
      return Result(user);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  Future<Result<User>> register(User user) async {
    try {

      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      Response response = await dio.post("${url_api}users", data: user.toJson(user));
      var jsonResponse = jsonDecode(response.data);
      var result = User.fromJson(jsonResponse);

      //print("Resultat : " + result.email);
      return Result(result);
    } on DioError catch (e) {
      if (e.response.statusCode == 400) {
        print("Erreur 400 : requête incorrecte");
      } else {
        print(e.toString() + ": PAS ERREUR de requête");
      }
    }
  }

  Future<void> uploadImage(File image) async {
    String imageName = image.path.split('/').last;
    FormData data = FormData.fromMap({
      "file": await MultipartFile.fromFile(
        image.path,
        filename: imageName,
        contentType: new MediaType("image", "jpeg"),
      )
    });
    try {
      Dio dio = new Dio();
      // dio.options.headers['content-Type'] = "multipart/form-data";
      //dio.options.headers["authorization"] = "Bearer $token";
      Response response = await dio.post("${url_api}media_objects", data: data);
      Map<String, dynamic> test = jsonDecode(response.data);
      print(test['@id']);
    } catch (e) {
      print(e.toString());
    }
  }

  Future<Result<User>> getProfilUser(User user, User currentUser) async {
    try {
      Dio dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      dio.options.headers["authorization"] = "Bearer ${currentUser.token}";
      print("LAUTRE USER : ${user.uuid}");
      print("MON USER : ${currentUser.id}");
      print("${url_api}users/${user.uuid}/get_infos");
      print("tok tok  ${currentUser.token}");
      Response response = await dio.get("${url_api}users/${user.uuid}/get_infos");
      var jsonResponse = jsonDecode(response.data);
      print(jsonResponse);
      var userResult = User.fromJson(jsonResponse);
      print(userResult);
      return Result(userResult);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  Future<Result<List<User>>> getListUser(User currentUser) async {
    try {
      Dio dio = new Dio();
      dio.options.headers['content-Type'] = "application/ld+json";
      dio.options.headers["authorization"] = "Bearer ${currentUser.token}";
      Response response = await dio.get("${url_api}users");
      var jsonResponse = jsonDecode(response.data);
      var userListResult = jsonResponse["hydra:member"]
          .map((e) => User.fromJson(e))
          .cast<User>()
          .toList();

      return Result(userListResult);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 400) {
        print(e);
        return Result.error("Erreur 400".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      print("PRINT ERREUR  : $e");
      return Result.error("Il y a un problème !");
    }
  }

  @override
  Future<Result<List<User>>> getListUserFilter(String filter, User currentUser) async {
    try {
      Dio dio = new Dio();
      dio.options.headers['content-Type'] = "application/ld+json";
      dio.options.headers["authorization"] = "Bearer ${currentUser.token}";
      Response response = await dio.get("${url_api}users?custom_user_filter=$filter");
      var jsonResponse = jsonDecode(response.data);
      var userListResult = jsonResponse["hydra:member"]
          .map((e) => User.fromJson(e))
          .cast<User>()
          .toList();

      return Result(userListResult);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 400) {
        print(e);
        return Result.error("Erreur 400".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      print("PRINT ERREUR  : $e");
      return Result.error("Il y a un problème !");
    }
  }
}
