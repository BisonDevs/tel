import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/ressource/repository_ressource.dart';
import 'package:ressource_relationnel/modele/catch_error.dart';
import 'package:ressource_relationnel/modele/ressource/ressource.dart';

part 'actualite_disconnect_controller.g.dart';

class ActualiteDisonnectController = _ActualiteDisconnectController
    with _$ActualiteDisonnectController;

abstract class _ActualiteDisconnectController with Store {
  RepositoryRessource repositoryRessource;

  _ActualiteDisconnectController(this.repositoryRessource){
    getListRessource();
  }

  @observable
  ObservableFuture listRessourceFuture;

  @observable
  List listRessource = List<Ressource>();

  @observable
  bool error = false;

  @observable
  String message="téléchargement...";

  @observable
  String errorMessage;

  @computed
  bool get isLoading {
    return listRessourceFuture?.status == FutureStatus.pending;
  }

  @action
  Future<void> getListRessource() async {
    error = false;
    listRessourceFuture =
        ObservableFuture(repositoryRessource.getListRessource());
    var listResult = await CatchError.handle(listRessourceFuture);
    if (listResult.isSuccess()) {
      listRessource = listResult.result;
    } else {
      error = true;
      //errorMessage = listResult.errorMessage;
      errorMessage = listResult.errorMessage;
    }
  }
}
