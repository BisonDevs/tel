import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/modele/ressource/ressource.dart';

part 'ressource_favoris_controller.g.dart';

class RessourceFavorisController = _RessourceFavorisController with _$RessourceFavorisController;

abstract class _RessourceFavorisController with Store{

  @observable
  List<Ressource> listeRessource = List();

  @observable
  bool error;

  @observable
  String message = "";

  @observable
  String errorMessage;

  @observable
  ObservableFuture requestFuture;

  @observable
  ObservableList<Ressource> listRessource = ObservableList<Ressource>();

  @computed
  bool get isLoading{
    return requestFuture?.status == FutureStatus.pending;
  }

  @action
  Future<bool> ajouterRessourcefavoris(Ressource ressource){

  }


  @action
  Future<bool> getListRessFav(){

  }

}

