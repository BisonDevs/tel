/*
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:html_editor/html_editor.dart';
import 'package:ressource_relationnel/controller/ressource_form/ressource_form_controller.dart';

import 'valide_ressource_dialog.dart';

class AjouterRessourcePage extends StatelessWidget {
  GlobalKey<HtmlEditorState> keyEditor = GlobalKey();
  RessourceFormController ressourceFormController;

  AjouterRessourcePage(this.ressourceFormController);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              HtmlEditor(
                hint: "Your text here...",
                key: keyEditor,
                height: MediaQuery.of(context).size.height * 0.85,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  RaisedButton(
                    child: Text(
                      "Suivant",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Color(0xff44dcc0),
                    onPressed: () {
                      ressourceFormController.htmlEditorState = keyEditor.currentState;
                      ValideRessourceDialog(
                        context: context,
                        ressourceFormController: ressourceFormController);}
                  ),
                  RaisedButton(
                      child: Text(
                        "retour",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.orange,
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
*/
import 'package:flutter/material.dart';
import 'package:ressource_relationnel/controller/ressource_form/ressource_form_controller.dart';
import 'package:ressource_relationnel/main.i18n.dart';

class AjouterRessourcePage extends StatelessWidget {
  RessourceFormController ressourceFormController;

  AjouterRessourcePage(this.ressourceFormController);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Text("Cette fonctionnalité est en cours de développement.".i18n,textAlign: TextAlign.center,)),
    );
  }
}
