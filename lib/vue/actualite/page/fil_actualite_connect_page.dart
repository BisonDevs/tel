import 'package:auto_size_text/auto_size_text.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:ressource_relationnel/controller/actualite/actualite_connect_controller.dart';
import 'package:ressource_relationnel/controller/core/core_controller.dart';
import 'package:provider/provider.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:ressource_relationnel/controller/user/user_controller.dart';
import 'package:ressource_relationnel/data/ressource/repository_ressource.dart';
import 'package:ressource_relationnel/vue/actualite/widget/action_bouton.dart';
import 'package:ressource_relationnel/vue/actualite/widget/contenu_ressource.dart';
import 'package:ressource_relationnel/vue/commun/loading_stack.dart';
import 'package:ressource_relationnel/vue/autre_profil/page/user_profil.dart';
import 'package:ressource_relationnel/main.i18n.dart';
import 'commentary_page.dart';

class FilActualiteConnectPage extends StatefulWidget {
  @override
  _FilActualiteConnectPageState createState() => _FilActualiteConnectPageState();
}

class _FilActualiteConnectPageState extends State<FilActualiteConnectPage> {
  ActualiteConnectController actualiteConnectController;
  UserController userController;
  CoreController coreController;

  @override
  initState() {
    actualiteConnectController = ActualiteConnectController(Provider.of<RepositoryRessourceAbstract>(context, listen: false));
    coreController = Provider.of<CoreController>(context, listen: false);
    userController = Provider.of<UserController>(context, listen: false);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await actualiteConnectController.getListRessourceConnect(coreController.user.token);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return LoadingStack(
        visible: actualiteConnectController.isLoading,
        message: actualiteConnectController.message,
        body: Scaffold(
          floatingActionButton: FloatingActionButton(
            heroTag: "btn1",
            onPressed: () {
              actualiteConnectController.getListRessourceConnect(coreController.user.token);
            },
            backgroundColor: Colors.blue,
            child: Icon(Icons.replay),
          ),
          body: ListView.builder(
            itemCount: actualiteConnectController.listRessource.length,
            itemBuilder: (context, index) {
              return Card(
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    ListTile(
                      leading: GestureDetector(
                        onTap: () {
                          userController.getUserProfil(actualiteConnectController.listRessource[index].createur).then((value) {
                            if (value) {
                              Flushbar(
                                title: "Une erreur s'est malheuresement produite".i18n,
                                message: userController.errorMessage,
                                duration: Duration(seconds: 3),
                              )..show(context);
                            } else {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => UserProfil(userController.user),
                                ),
                              );
                            }
                          });
                        },
                        child: CircleAvatar(
                          radius: 20,
                          backgroundColor: Colors.blue,
                        ),
                      ),
                      title: AutoSizeText(
                        '${actualiteConnectController.listRessource[index].titre}',
                        maxLines: 2,
                      ),
                      subtitle: AutoSizeText(
                        '${actualiteConnectController.listRessource[index].createur.nom.toUpperCase()} ${actualiteConnectController.listRessource[index].createur.prenom}',
                        style: TextStyle(color: Colors.black.withOpacity(0.6)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: ContenuRessource(actualiteConnectController.listRessource[index]),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 200,
                      child: Icon(Icons.note_add_rounded),
                    ),
                    ButtonBar(
                      alignment: MainAxisAlignment.start,
                      children: [
                        FlatButton(
                          child: ActionBouton(actualiteConnectController.listRessource[index]),
                          onPressed: () {},
                          textColor: Color(0xff44dcc0),
                        ),
                        FlatButton(
                          textColor: Color(0xff44dcc0),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => CommentaryPage()));
                          },
                          child: AutoSizeText('Commenter', maxLines: 1),
                        ),
                        IconButton(
                          icon: Icon(Icons.attachment),
                          onPressed: () {},
                          color: Color(0xff44dcc0),
                        ),
                        IconButton(
                          icon: Icon(Icons.assistant_direction),
                          onPressed: () {},
                          color: Color(0xff44dcc0),
                        ),
                      ],
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      );
    });
  }
}
