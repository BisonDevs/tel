import 'package:flutter/material.dart';
import 'package:ressource_relationnel/controller/core/core_controller.dart';
import 'package:ressource_relationnel/data/user/repository_user.dart';
import 'package:ressource_relationnel/vue/home_page.dart';
import 'package:ressource_relationnel/controller/login/login_controller.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/vue/user/page/connexion_page.dart';
import 'package:ressource_relationnel/main.i18n.dart';
import 'package:ressource_relationnel/vue/user/page/register_page.dart';

class ParametrePage extends StatefulWidget {
  @override
  _ParametrePageState createState() => _ParametrePageState();
}

class _ParametrePageState extends State<ParametrePage> {
  LoginController loginController;

  CoreController coreController;

  @override
  void initState() {
    loginController = LoginController(
        Provider.of<RepositoryUserAbstract>(context, listen: false));
    coreController = Provider.of<CoreController>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.grey[400], width: 0.5),
                color: Colors.white,
              ),
              padding: EdgeInsets.all(9),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    child: FlatButton(
                      child: ListTile(
                        title: Text(
                          coreController.isConnect
                              ? "Déconnection".i18n
                              : "Connection".i18n,
                          style: coreController.isConnect
                              ? TextStyle(color: Colors.red)
                              : TextStyle(color: Colors.blue),
                        ),
                        subtitle: Text(
                          coreController.isConnect
                              ? "Appuyer pour vous déconnecter...".i18n
                              : "Appuyer pour vous connecter...".i18n,
                          style: TextStyle(fontSize: 10),
                        ),
                        trailing: coreController.isConnect
                            ? Icon(
                                Icons.logout,
                                color: Colors.red,
                              )
                            : Icon(
                                Icons.login,
                                color: Colors.blue,
                              ),
                      ),
                      onPressed: coreController.isConnect
                          ? () {
                              coreController.setIsConnect(false);
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => HomePage()),
                                  (Route<dynamic> route) => false);
                            }
                          : () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ConnexionPage()));
                            },
                    ),
                  ),
                  Divider(
                    color: Colors.grey[300],
                    height: 5,
                    indent: 15,
                    endIndent: 15,
                    thickness: 1,
                  ),
                  Container(
                    child: FlatButton(
                      child: ListTile(
                        title: Text(
                          "Créer un compte".i18n,
                          style: TextStyle(color: Colors.green),
                        ),
                        subtitle: Text(
                          "Appuyer pour vous créer un compte...".i18n,
                          style: TextStyle(fontSize: 10),
                        ),
                        trailing: Icon(
                          Icons.supervised_user_circle,
                          color: Colors.green,
                        ),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RegisterPage()));
                      },
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.grey[400], width: 0.5),
                color: Colors.white,
              ),
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.all(20),
                    child: FlatButton(
                      child: Text("Test Deuxième partie".i18n),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
