import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:ressource_relationnel/modele/relation/relation.dart';
import 'package:ressource_relationnel/main.i18n.dart';
import 'package:ressource_relationnel/vue/relation/widget/supprimer_dialog.dart';

class ListRelation extends StatelessWidget {
  Relation relation;
  var controller;
  bool editMode;
  bool accepteMode;

  ListRelation(
      {this.relation, this.controller, this.accepteMode, this.editMode});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.grey[400], width: 0.5),
        color: Colors.white,
      ),
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        actionExtentRatio: 0.25,
        child: Container(
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.blue,
            ),
            title: AutoSizeText(
              '${relation.demandeur.prenom} ${relation.demandeur.nom}',
              maxLines: 2,
            ),
            subtitle: AutoSizeText(
              'Demande une relation de type : ${relation.id}',
              maxLines: 3,
            ),
          ),
        ),
        actions: accepteMode == false
            ? [
                editMode
                    ? IconSlideAction(
                        caption: 'Modifier'.i18n,
                        color: Colors.blue,
                        icon: Icons.carpenter,
                        onTap: () {},
                      )
                    : null
              ]
            : null,
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: 'Accepter'.i18n,
            color: Colors.green,
            icon: Icons.add_circle_outline,
            onTap: () {},
          ),
          IconSlideAction(
            caption: 'Supprimer'.i18n,
            color: Colors.red,
            icon: Icons.delete,
            onTap: () {
              SupprimerDialog(
                  context: context, controller: controller, relation: relation);
            },
          ),
        ],
      ),
    );
  }
}
