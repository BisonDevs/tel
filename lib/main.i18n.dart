import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static var _t;

  String get i18n => localize(this, _t);

  String fill(List<Object> params) => localizeFill(this, params);

  static Future load() async {
    String frJsonString = await rootBundle.loadString("assets/i18n/fr_FR.json");
    String enJsonString = await rootBundle.loadString("assets/i18n/en_US.json");
    _t = Translations.byLocale("fr_fr") +
        {
          "fr_fr": Map<String, String>.from(jsonDecode(frJsonString)),
        } +
        {
          "en_us": Map<String, String>.from(jsonDecode(enJsonString)),
        };
    return;
  }
}
