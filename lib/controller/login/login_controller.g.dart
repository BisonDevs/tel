// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LoginController on _LoginController, Store {
  Computed<bool> _$authentificationIsLoadingComputed;

  @override
  bool get authentificationIsLoading => (_$authentificationIsLoadingComputed ??=
          Computed<bool>(() => super.authentificationIsLoading,
              name: '_LoginController.authentificationIsLoading'))
      .value;

  final _$userTokenAtom = Atom(name: '_LoginController.userToken');

  @override
  String get userToken {
    _$userTokenAtom.reportRead();
    return super.userToken;
  }

  @override
  set userToken(String value) {
    _$userTokenAtom.reportWrite(value, super.userToken, () {
      super.userToken = value;
    });
  }

  final _$userAtom = Atom(name: '_LoginController.user');

  @override
  User get user {
    _$userAtom.reportRead();
    return super.user;
  }

  @override
  set user(User value) {
    _$userAtom.reportWrite(value, super.user, () {
      super.user = value;
    });
  }

  final _$emailAtom = Atom(name: '_LoginController.email');

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  final _$passwordAtom = Atom(name: '_LoginController.password');

  @override
  String get password {
    _$passwordAtom.reportRead();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.reportWrite(value, super.password, () {
      super.password = value;
    });
  }

  final _$msgAtom = Atom(name: '_LoginController.msg');

  @override
  String get msg {
    _$msgAtom.reportRead();
    return super.msg;
  }

  @override
  set msg(String value) {
    _$msgAtom.reportWrite(value, super.msg, () {
      super.msg = value;
    });
  }

  final _$errorMsgAtom = Atom(name: '_LoginController.errorMsg');

  @override
  String get errorMsg {
    _$errorMsgAtom.reportRead();
    return super.errorMsg;
  }

  @override
  set errorMsg(String value) {
    _$errorMsgAtom.reportWrite(value, super.errorMsg, () {
      super.errorMsg = value;
    });
  }

  final _$errorAtom = Atom(name: '_LoginController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$authentificationFutureAtom =
      Atom(name: '_LoginController.authentificationFuture');

  @override
  ObservableFuture<dynamic> get authentificationFuture {
    _$authentificationFutureAtom.reportRead();
    return super.authentificationFuture;
  }

  @override
  set authentificationFuture(ObservableFuture<dynamic> value) {
    _$authentificationFutureAtom
        .reportWrite(value, super.authentificationFuture, () {
      super.authentificationFuture = value;
    });
  }

  final _$listUserAtom = Atom(name: '_LoginController.listUser');

  @override
  ObservableList<User> get listUser {
    _$listUserAtom.reportRead();
    return super.listUser;
  }

  @override
  set listUser(ObservableList<User> value) {
    _$listUserAtom.reportWrite(value, super.listUser, () {
      super.listUser = value;
    });
  }

  final _$removeUserSaveAtIndexAsyncAction =
      AsyncAction('_LoginController.removeUserSaveAtIndex');

  @override
  Future<void> removeUserSaveAtIndex(dynamic index) {
    return _$removeUserSaveAtIndexAsyncAction
        .run(() => super.removeUserSaveAtIndex(index));
  }

  final _$saveUserAsyncAction = AsyncAction('_LoginController.saveUser');

  @override
  Future<bool> saveUser() {
    return _$saveUserAsyncAction.run(() => super.saveUser());
  }

  final _$getListUserSaveAsyncAction =
      AsyncAction('_LoginController.getListUserSave');

  @override
  Future<dynamic> getListUserSave() {
    return _$getListUserSaveAsyncAction.run(() => super.getListUserSave());
  }

  final _$authentificationAsyncAction =
      AsyncAction('_LoginController.authentification');

  @override
  Future<bool> authentification(FormBuilderState state) {
    return _$authentificationAsyncAction
        .run(() => super.authentification(state));
  }

  final _$authentification_autoAsyncAction =
      AsyncAction('_LoginController.authentification_auto');

  @override
  Future<bool> authentification_auto(String email, String password) {
    return _$authentification_autoAsyncAction
        .run(() => super.authentification_auto(email, password));
  }

  final _$_LoginControllerActionController =
      ActionController(name: '_LoginController');

  @override
  bool verifFirstTime() {
    final _$actionInfo = _$_LoginControllerActionController.startAction(
        name: '_LoginController.verifFirstTime');
    try {
      return super.verifFirstTime();
    } finally {
      _$_LoginControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
userToken: ${userToken},
user: ${user},
email: ${email},
password: ${password},
msg: ${msg},
errorMsg: ${errorMsg},
error: ${error},
authentificationFuture: ${authentificationFuture},
listUser: ${listUser},
authentificationIsLoading: ${authentificationIsLoading}
    ''';
  }
}
