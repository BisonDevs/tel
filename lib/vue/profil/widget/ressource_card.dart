import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:ressource_relationnel/modele/ressource/ressource.dart';
import 'package:ressource_relationnel/main.i18n.dart';
import 'package:ressource_relationnel/vue/actualite/widget/action_bouton.dart';
import 'package:ressource_relationnel/vue/actualite/widget/contenu_ressource.dart';

class RessourceCard extends StatelessWidget {
  Ressource ressource;

  RessourceCard(this.ressource);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.grey[400], width: 0.5),
        color: Colors.white,
      ),
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        actionExtentRatio: 0.25,
        child: Container(
          child: ListTile(
            leading: ActionBouton(ressource),
            title: AutoSizeText(
              '${ressource.titre}',
              maxLines: 1,
            ),
            subtitle: ContenuRessource(ressource),
          ),
        ),
        actions: <Widget>[
          IconSlideAction(
            caption: 'Partager'.i18n,
            color: Colors.indigo,
            icon: Icons.share,
            onTap: () {},
          ),
        ],
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: 'Supprimer'.i18n,
            color: Colors.red,
            icon: Icons.delete,
            onTap: () {
              return showDialog<void>(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return CupertinoAlertDialog(
                    title: Text("Êtes vous sur de vouloir supprimer votre ressource ?".i18n),
                    actions: [
                      TextButton(
                        child: Text('Accepter'.i18n),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
