import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ressource_relationnel/controller/core/core_controller.dart';
import 'package:ressource_relationnel/vue/profil/page/profil_connecte.dart';
import 'package:ressource_relationnel/vue/profil/page/profil_non_connecte.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

class MaPageProfil extends StatefulWidget {
  @override
  _MaPageProfilState createState() => _MaPageProfilState();
}

class _MaPageProfilState extends State<MaPageProfil> {
  CoreController coreController;

  @override
  void initState() {
    coreController = Provider.of<CoreController>(context,listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return Scaffold(
        body: coreController.isConnect
            ? ProfilConnecte()
            : ProfilNonConnecte()
      );
    });
  }
}
