import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:ressource_relationnel/modele/ressource/ressource.dart';
import 'package:ressource_relationnel/vue/actualite/widget/ressource_page.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:ressource_relationnel/main.i18n.dart';

class ActionBouton extends StatelessWidget {
  Ressource ressource;

  ActionBouton(this.ressource);

  @override
  Widget build(BuildContext context) {
    Future<void> _launchInBrowser(String url) async {
      print(url);
      if (await canLaunch(url)) {
        await launch(
          url,
          forceSafariVC: false,
          forceWebView: false,
          headers: <String, String>{'my_header_key': 'my_header_value'},
        );
      } else {
        throw 'Could not launch $url';
      }
    }

    switch (ressource.contenu.keys.toString()) {
      case "(url)":
        {
          return InkWell(
            child: AutoSizeText(
              "Visionner".i18n,
            ),
            onTap: () => _launchInBrowser(ressource.contenu["url"]),
          );
        }
        break;

      case "(text)":
        {
          return InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => RessourcePage()));
              },
              child: AutoSizeText("Voir"));
        }
        break;

      case "(text, url)":
        {
          return InkWell(
            child: AutoSizeText(
              "Visionner".i18n,
            ),
            onTap: () => _launchInBrowser(ressource.contenu["url"]),
          );
        }
        break;
      case "(url, media)":
        {
          return InkWell(
            child: AutoSizeText(
              "Visionner".i18n,
            ),
            onTap: () => _launchInBrowser("${ressource.contenu["media"]}"),
          );
        }
        break;
      default:
        {
          return AutoSizeText(
            "Visionner".i18n,
          );
        }
    }
  }
}
