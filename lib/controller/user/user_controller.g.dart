// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$UserController on _UserController, Store {
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_UserController.isLoading'))
          .value;
  Computed<bool> _$UserListIsLoadingComputed;

  @override
  bool get UserListIsLoading => (_$UserListIsLoadingComputed ??= Computed<bool>(
          () => super.UserListIsLoading,
          name: '_UserController.UserListIsLoading'))
      .value;

  final _$errorMessageAtom = Atom(name: '_UserController.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$messageAtom = Atom(name: '_UserController.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$errorAtom = Atom(name: '_UserController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$requestUserProfilFutureAtom =
      Atom(name: '_UserController.requestUserProfilFuture');

  @override
  ObservableFuture<dynamic> get requestUserProfilFuture {
    _$requestUserProfilFutureAtom.reportRead();
    return super.requestUserProfilFuture;
  }

  @override
  set requestUserProfilFuture(ObservableFuture<dynamic> value) {
    _$requestUserProfilFutureAtom
        .reportWrite(value, super.requestUserProfilFuture, () {
      super.requestUserProfilFuture = value;
    });
  }

  final _$userAtom = Atom(name: '_UserController.user');

  @override
  User get user {
    _$userAtom.reportRead();
    return super.user;
  }

  @override
  set user(User value) {
    _$userAtom.reportWrite(value, super.user, () {
      super.user = value;
    });
  }

  final _$requestListUsersFutureAtom =
      Atom(name: '_UserController.requestListUsersFuture');

  @override
  ObservableFuture<dynamic> get requestListUsersFuture {
    _$requestListUsersFutureAtom.reportRead();
    return super.requestListUsersFuture;
  }

  @override
  set requestListUsersFuture(ObservableFuture<dynamic> value) {
    _$requestListUsersFutureAtom
        .reportWrite(value, super.requestListUsersFuture, () {
      super.requestListUsersFuture = value;
    });
  }

  final _$usersAtom = Atom(name: '_UserController.users');

  @override
  List<User> get users {
    _$usersAtom.reportRead();
    return super.users;
  }

  @override
  set users(List<User> value) {
    _$usersAtom.reportWrite(value, super.users, () {
      super.users = value;
    });
  }

  final _$pickerAtom = Atom(name: '_UserController.picker');

  @override
  ImagePicker get picker {
    _$pickerAtom.reportRead();
    return super.picker;
  }

  @override
  set picker(ImagePicker value) {
    _$pickerAtom.reportWrite(value, super.picker, () {
      super.picker = value;
    });
  }

  final _$imageAtom = Atom(name: '_UserController.image');

  @override
  File get image {
    _$imageAtom.reportRead();
    return super.image;
  }

  @override
  set image(File value) {
    _$imageAtom.reportWrite(value, super.image, () {
      super.image = value;
    });
  }

  final _$getImageAsyncAction = AsyncAction('_UserController.getImage');

  @override
  Future<dynamic> getImage() {
    return _$getImageAsyncAction.run(() => super.getImage());
  }

  final _$uploadFileAsyncAction = AsyncAction('_UserController.uploadFile');

  @override
  Future<dynamic> uploadFile(File image) {
    return _$uploadFileAsyncAction.run(() => super.uploadFile(image));
  }

  final _$getUserProfilAsyncAction =
      AsyncAction('_UserController.getUserProfil');

  @override
  Future getUserProfil(User choiceUser) {
    return _$getUserProfilAsyncAction
        .run(() => super.getUserProfil(choiceUser));
  }

  final _$getListUsersAsyncAction = AsyncAction('_UserController.getListUsers');

  @override
  Future<bool> getListUsers() {
    return _$getListUsersAsyncAction.run(() => super.getListUsers());
  }

  final _$filterListUsersAsyncAction =
      AsyncAction('_UserController.filterListUsers');

  @override
  Future<List<User>> filterListUsers(String text) {
    return _$filterListUsersAsyncAction.run(() => super.filterListUsers(text));
  }

  final _$_UserControllerActionController =
      ActionController(name: '_UserController');

  @override
  dynamic setCurrentUser(User newUser) {
    final _$actionInfo = _$_UserControllerActionController.startAction(
        name: '_UserController.setCurrentUser');
    try {
      return super.setCurrentUser(newUser);
    } finally {
      _$_UserControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
errorMessage: ${errorMessage},
message: ${message},
error: ${error},
requestUserProfilFuture: ${requestUserProfilFuture},
user: ${user},
requestListUsersFuture: ${requestListUsersFuture},
users: ${users},
picker: ${picker},
image: ${image},
isLoading: ${isLoading},
UserListIsLoading: ${UserListIsLoading}
    ''';
  }
}
