// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ressource_favoris_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RessourceFavorisController on _RessourceFavorisController, Store {
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_RessourceFavorisController.isLoading'))
          .value;

  final _$listeRessourceAtom =
      Atom(name: '_RessourceFavorisController.listeRessource');

  @override
  List<Ressource> get listeRessource {
    _$listeRessourceAtom.reportRead();
    return super.listeRessource;
  }

  @override
  set listeRessource(List<Ressource> value) {
    _$listeRessourceAtom.reportWrite(value, super.listeRessource, () {
      super.listeRessource = value;
    });
  }

  final _$errorAtom = Atom(name: '_RessourceFavorisController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$messageAtom = Atom(name: '_RessourceFavorisController.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$errorMessageAtom =
      Atom(name: '_RessourceFavorisController.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$requestFutureAtom =
      Atom(name: '_RessourceFavorisController.requestFuture');

  @override
  ObservableFuture<dynamic> get requestFuture {
    _$requestFutureAtom.reportRead();
    return super.requestFuture;
  }

  @override
  set requestFuture(ObservableFuture<dynamic> value) {
    _$requestFutureAtom.reportWrite(value, super.requestFuture, () {
      super.requestFuture = value;
    });
  }

  final _$listRessourceAtom =
      Atom(name: '_RessourceFavorisController.listRessource');

  @override
  ObservableList<Ressource> get listRessource {
    _$listRessourceAtom.reportRead();
    return super.listRessource;
  }

  @override
  set listRessource(ObservableList<Ressource> value) {
    _$listRessourceAtom.reportWrite(value, super.listRessource, () {
      super.listRessource = value;
    });
  }

  final _$_RessourceFavorisControllerActionController =
      ActionController(name: '_RessourceFavorisController');

  @override
  Future<bool> ajouterRessourcefavoris(Ressource ressource) {
    final _$actionInfo =
        _$_RessourceFavorisControllerActionController.startAction(
            name: '_RessourceFavorisController.ajouterRessourcefavoris');
    try {
      return super.ajouterRessourcefavoris(ressource);
    } finally {
      _$_RessourceFavorisControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  Future<bool> getListRessFav() {
    final _$actionInfo = _$_RessourceFavorisControllerActionController
        .startAction(name: '_RessourceFavorisController.getListRessFav');
    try {
      return super.getListRessFav();
    } finally {
      _$_RessourceFavorisControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
listeRessource: ${listeRessource},
error: ${error},
message: ${message},
errorMessage: ${errorMessage},
requestFuture: ${requestFuture},
listRessource: ${listRessource},
isLoading: ${isLoading}
    ''';
  }
}
