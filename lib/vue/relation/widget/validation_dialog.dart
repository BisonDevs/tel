import 'package:auto_size_text/auto_size_text.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/relation_form/relation_form_controller.dart';
import 'package:ressource_relationnel/data/relation/repository_relation.dart';
import 'package:ressource_relationnel/modele/type_relation/type_relation.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/main.i18n.dart';

Future<void> ValidationDialog(
    {BuildContext context,
    User targetUser,
    User currentUser,
    TypeRelation typeRelation}) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      RelationFormController relationFormController = RelationFormController(
          Provider.of<RepositoryRelationAbstract>(context, listen: false));
      return AlertDialog(
        title: Text('Ajouter une relation'.i18n),
        content: Container(
          child: AutoSizeText("Voulez-vous envoyer une demande de relation ? ".i18n),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Ajouter'.i18n),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: Text('Enregistrer'.i18n),
            onPressed: () {
              relationFormController.ajouterRelation(
                targetUser: targetUser,
                currentUser: currentUser,
                typeRelation: typeRelation,
              ).then((value){
                if(value){
                  Navigator.pop(context);
                }else{
                  Navigator.pop(context);
                  Flushbar(
                    title: "Une erreur s'est malheuresement produite",
                    messageText: Text(
                      "La relation a correctement été effacée",
                      style: TextStyle(color: Colors.red),
                    ),
                    duration: Duration(seconds: 2),
                  )..show(context);
                }
              });
            },
          ),
        ],
      );
    },
  );
}
