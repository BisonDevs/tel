import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/vue/relation/page/mes_demandes_relations_page.dart';
import 'package:ressource_relationnel/vue/relation/page/relation_attente_page.dart';
import 'package:ressource_relationnel/vue/relation/page/relation_connect_page.dart';

part 'relation_navigation_controller.g.dart';

class RelationNavigationController = _RelationNavigationController with _$RelationNavigationController;

abstract class _RelationNavigationController with Store {
  User currentUser;


  _RelationNavigationController(this.currentUser);

  @observable
  int tabItemSelected = 0;

  @action
  void changeTabSelected(int index) => tabItemSelected = index;

  @action
  Widget displayWidget(){
    List<Widget> listPages = <Widget>[
      RelationConnectPage(),
      MesDemandesRelationsPage(currentUser: currentUser,),
      RelationAttentePage(currentUser : currentUser),
    ];

    return listPages[tabItemSelected];
  }
}
