import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/relation/repository_relation.dart';
import 'package:ressource_relationnel/modele/catch_error.dart';
import 'package:ressource_relationnel/modele/type_relation/type_relation.dart';
import 'package:ressource_relationnel/modele/user/user.dart';

part 'relation_form_controller.g.dart';

class RelationFormController = _RelationFormController
    with _$RelationFormController;

abstract class _RelationFormController with Store {
  RepositoryRelation repositoryRelation;

  _RelationFormController(this.repositoryRelation);

  @observable
  bool error;

  @observable
  String message = "En cours ...";

  @observable
  String errorMessage;

  @observable
  ObservableFuture requestFuture;

  @computed
  bool get isLoading {
    return requestFuture?.status == FutureStatus.pending;
  }

  @action
  Future<bool> ajouterRelation(
      {User targetUser, User currentUser, TypeRelation typeRelation}) async {
    error = false;
    requestFuture = ObservableFuture(repositoryRelation.ajouterRelation(
        currentUser: currentUser,
        targetUser: targetUser,
        typeRelation: typeRelation));
    var requestResult = await CatchError.handle(requestFuture);
    if(requestResult.isSuccess()){
      error = requestResult.result;
      return error;
    }else{
      error = requestResult.result;
      errorMessage = requestResult.errorMessage;
      return error;
    }
  }
}
