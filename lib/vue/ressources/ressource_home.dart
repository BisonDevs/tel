import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/categorie_ressource/categorie_ressource_controller.dart';
import 'package:ressource_relationnel/controller/core/core_controller.dart';
import 'package:ressource_relationnel/controller/ressource_form/ressource_form_controller.dart';
import 'package:ressource_relationnel/controller/type_ressource/type_ressource_controller.dart';
import 'package:ressource_relationnel/data/ressource/repository_ressource.dart';
import 'package:ressource_relationnel/vue/commun/loading_stack.dart';
import 'package:ressource_relationnel/vue/ressources/page/type_ressource/type_ressource_page.dart';

class RessourceHome extends StatefulWidget {
  @override
  _RessourceHomeState createState() => _RessourceHomeState();
}

class _RessourceHomeState extends State<RessourceHome> {
  CoreController coreController;
  TypeRessourceController typeRessourceController;
  CategorieRessourceController categorieRessourceController;
  RessourceFormController ressourceFormController;

  @override
  void initState() {
    typeRessourceController = TypeRessourceController(Provider.of<RepositoryRessourceAbstract>(context, listen: false));
    categorieRessourceController =
        CategorieRessourceController(Provider.of<RepositoryRessourceAbstract>(context, listen: false));
    coreController = Provider.of<CoreController>(context, listen: false);
    ressourceFormController = RessourceFormController(
        Provider.of<RepositoryRessourceAbstract>(context, listen: false));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return Scaffold(
          body: LoadingStack(
            message: typeRessourceController.message,
            visible: typeRessourceController.isLoading,
            body: Provider.value(
              value: ressourceFormController,
              child: TypeRessourcePage(
                  typeRessourceController, categorieRessourceController),
            ),
          ));
    });
  }
}
