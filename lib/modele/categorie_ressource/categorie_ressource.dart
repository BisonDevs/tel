class CategorieRessource{
  String id;
  String slug;
  String libelle;
  List lsRessources;

  CategorieRessource({this.id, this.slug, this.libelle, this.lsRessources});
  factory CategorieRessource.fromJson(Map<String,dynamic> json) {
    return CategorieRessource(
      id: json['@id'] as String,
      slug: json['slug'] as String,
      libelle: json['libelle'] as String,
      lsRessources: json['lsRessources'] as List,
    );
  }
  Map<String,dynamic> toJson(CategorieRessource instance) =>
      <String, dynamic>{
        '@id': instance.id,
        'slug': instance.slug,
        'libelle': instance.libelle,
        'lsRessources': instance.lsRessources,
      };
}