import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/ressource/repository_ressource.dart';
import 'package:ressource_relationnel/modele/catch_error.dart';
import 'package:ressource_relationnel/modele/categorie_ressource/categorie_ressource.dart';
import 'package:ressource_relationnel/main.i18n.dart';

part 'categorie_ressource_controller.g.dart';

class CategorieRessourceController = _CategorieRessourceController with _$CategorieRessourceController;

abstract class _CategorieRessourceController with Store {

  RepositoryRessource repositoryRessource;

  _CategorieRessourceController(repositoryRessource){
    this.repositoryRessource = repositoryRessource;
    getListCategorieRessource();
  }

  @observable
  List<CategorieRessource> listCategorieRessource = new List();

  // Le choix de la catégorie de ressource qui va être crée
  @observable
  String idCatRessSel = null;

  @observable
  bool error = false;

  @observable
  String message = "Téléchargement...".i18n;

  @observable
  String errorMessage;

  @observable
  ObservableFuture listobsFuture;

  @computed
  bool get isLoading{
    return listobsFuture?.status == FutureStatus.pending;
  }

  @action
  Future getListCategorieRessource() async {
    error = false;
    listobsFuture = ObservableFuture(repositoryRessource.getListCategorieRessource());
    var listResult = await CatchError.handle(listobsFuture);
    if(listResult.isSuccess()){
      listCategorieRessource = listResult.result;
    }else{
      error = false;
      errorMessage = listResult.errorMessage;
    }

  }

  @action
  void setIdCategorieRessSel(String newId){
    idCatRessSel = newId;
    repositoryRessource.idCategorieRessToCreate = idCatRessSel;
  }


}