// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'type_ressource_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$TypeRessourceController on _TypeRessourceController, Store {
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_TypeRessourceController.isLoading'))
          .value;

  final _$listTypeRessourceAtom =
      Atom(name: '_TypeRessourceController.listTypeRessource');

  @override
  List<TypeRessource> get listTypeRessource {
    _$listTypeRessourceAtom.reportRead();
    return super.listTypeRessource;
  }

  @override
  set listTypeRessource(List<TypeRessource> value) {
    _$listTypeRessourceAtom.reportWrite(value, super.listTypeRessource, () {
      super.listTypeRessource = value;
    });
  }

  final _$messageAtom = Atom(name: '_TypeRessourceController.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$errorAtom = Atom(name: '_TypeRessourceController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$errorMessageAtom =
      Atom(name: '_TypeRessourceController.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$idTypeRessSelAtom =
      Atom(name: '_TypeRessourceController.idTypeRessSel');

  @override
  String get idTypeRessSel {
    _$idTypeRessSelAtom.reportRead();
    return super.idTypeRessSel;
  }

  @override
  set idTypeRessSel(String value) {
    _$idTypeRessSelAtom.reportWrite(value, super.idTypeRessSel, () {
      super.idTypeRessSel = value;
    });
  }

  final _$listTypeFutureAtom =
      Atom(name: '_TypeRessourceController.listTypeFuture');

  @override
  ObservableFuture<dynamic> get listTypeFuture {
    _$listTypeFutureAtom.reportRead();
    return super.listTypeFuture;
  }

  @override
  set listTypeFuture(ObservableFuture<dynamic> value) {
    _$listTypeFutureAtom.reportWrite(value, super.listTypeFuture, () {
      super.listTypeFuture = value;
    });
  }

  final _$getListTypeRessourceAsyncAction =
      AsyncAction('_TypeRessourceController.getListTypeRessource');

  @override
  Future<dynamic> getListTypeRessource() {
    return _$getListTypeRessourceAsyncAction
        .run(() => super.getListTypeRessource());
  }

  final _$_TypeRessourceControllerActionController =
      ActionController(name: '_TypeRessourceController');

  @override
  void setIdTypeRessSel(String newId) {
    final _$actionInfo = _$_TypeRessourceControllerActionController.startAction(
        name: '_TypeRessourceController.setIdTypeRessSel');
    try {
      return super.setIdTypeRessSel(newId);
    } finally {
      _$_TypeRessourceControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
listTypeRessource: ${listTypeRessource},
message: ${message},
error: ${error},
errorMessage: ${errorMessage},
idTypeRessSel: ${idTypeRessSel},
listTypeFuture: ${listTypeFuture},
isLoading: ${isLoading}
    ''';
  }
}
