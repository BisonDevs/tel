import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/ressource/repository_ressource.dart';
part 'ressource_form_controller.g.dart';

class RessourceFormController = _RessourceFormController with _$RessourceFormController;

abstract class _RessourceFormController with Store{
  RepositoryRessourceAbstract repositoryRessourceAbstract;


  _RessourceFormController(this.repositoryRessourceAbstract);

  @observable
  String typeRessourceId ="";

  @observable
  String categorieRessourceId = "";

  @observable
  bool error;

  @observable
  String message = "En cours ...";

  @observable
  String errorMessage;

  @observable
  ObservableFuture requestFuture;

//  HtmlEditorState htmlEditorState;

  String titre;


  @computed
  bool get isLoading {
    return requestFuture?.status == FutureStatus.pending;
  }

  /*@action
  Future<bool> creationRessource()async{
      String text = await htmlEditorState.getText();
      print("LE CONTENUE DE LA RESSOURCE  : $text");
      print("type ressource id = $typeRessourceId");
      print("type categorie id = $categorieRessourceId");
      RessourceApi ressourceApi = RessourceApi(categorieRessource: categorieRessourceId, typeRessource: typeRessourceId,contenu: text);
      requestFuture = ObservableFuture(repositoryRessourceAbstract.ajouterRessource(ressourceApi : ressourceApi));
      var requestResult = await CatchError.handle(requestFuture);
      if (requestResult.isSuccess()){

      }else{

      }
  }*/

}