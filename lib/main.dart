import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/core/core_controller.dart';
import 'package:ressource_relationnel/data/relation/repository_relation.dart';
import 'package:ressource_relationnel/data/ressource/repository_ressource.dart';
import 'package:ressource_relationnel/data/user/repository_user.dart';
import 'package:ressource_relationnel/vue/home_page.dart';
import 'controller/login/login_controller.dart';
import 'controller/navigation/navigation_controller.dart';
import 'controller/user/user_controller.dart';
import 'main.i18n.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Localization.load();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  NavigationController navigationController;
  LoginController loginController;
  UserController userController;
  CoreController coreController;
  RepositoryUserAbstract repositoryUser;
  RepositoryRessourceAbstract repositoryRessource;
  RepositoryRelationAbstract repositoryRelation;

  @override
  void initState() {
    repositoryUser = RepositoryUser();
    repositoryRessource = RepositoryRessource();
    repositoryRelation = RepositoryRelation();
    navigationController = NavigationController();
    userController = UserController(repositoryUser);
    coreController = CoreController(repositoryUser);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider.value(
          value: navigationController,
        ),
        Provider.value(
          value: repositoryRelation,
        ),
        Provider.value(
          value: coreController,
        ),
        Provider.value(
          value: repositoryUser,
        ),
        Provider.value(
          value: repositoryRessource,
        ),
        ProxyProvider<RepositoryUserAbstract, UserController>(
          update: (context, repositoryUser, userController) =>
              UserController(repositoryUser),
        ),
        ProxyProvider<RepositoryUserAbstract, CoreController>(
          update: (context, repositoryUser, coreController) =>
              CoreController(repositoryUser),
        )
      ],
      child: MaterialApp(
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          localeResolutionCallback:
              (Locale locale, Iterable<Locale> supportedLocales) {
            for (Locale supportedLocale in supportedLocales) {
              if (locale.languageCode == supportedLocale.languageCode) {
                return locale;
              }
            }
            return Locale('en', 'US');
          },
          supportedLocales: [
            const Locale('en', "US"),
            const Locale('fr', "FR"),
          ],
          home: I18n(child: HomePage())),
    );
  }
}
