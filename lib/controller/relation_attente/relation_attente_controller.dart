import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/relation/repository_relation.dart';
import 'package:ressource_relationnel/modele/catch_error.dart';
import 'package:ressource_relationnel/modele/relation/relation.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/main.i18n.dart';
import 'package:ressource_relationnel/modele_api/etat_relation_api/etat_relation_api.dart';

part 'relation_attente_controller.g.dart';

class RelationAttenteController = _RelationAttenteController
    with _$RelationAttenteController;

abstract class _RelationAttenteController with Store {
  RepositoryRelationAbstract repositoryRelationAbstract;
  User currentUser;

  _RelationAttenteController(
      this.repositoryRelationAbstract, this.currentUser) {
    getListMesDemandes();
  }

  @observable
  bool error = false;

  @observable
  String errorMessage;

  @observable
  String message = "Téléchargement...".i18n;

  @observable
  ObservableFuture requestFuture;

  @observable
  ObservableFuture decisionFuture;

  @observable
  ObservableList<Relation> listRelation = ObservableList<Relation>();

  @computed
  bool get isLoading {
    return requestFuture?.status == FutureStatus.pending;
  }

  Future<bool> getListMesDemandes() async {
    error = false;
    requestFuture = ObservableFuture(repositoryRelationAbstract
        .getListDemandesEnAttente(currentUser: currentUser));
    var listRelationResult = await CatchError.handle(requestFuture);
    if (listRelationResult.isSuccess()) {
      error = false;
      listRelation = ObservableList.of(listRelationResult.result);
      print(listRelation.length);
      return error;
    } else {
      error = true;
      errorMessage = listRelationResult.errorMessage;
      return error;
    }
  }

  @action
  Future<bool> decisionDemande({Relation relation, EtatRelationApi etat}) async {
    decisionFuture = ObservableFuture(
        repositoryRelationAbstract.decisionRelation(
            relation: relation, token: currentUser.token, etat: etat));
    var resultRequest = await CatchError.handle(decisionFuture);
    if (resultRequest.isSuccess()) {
      listRelation.removeWhere((element) => element.id == relation.id);
      return true;
    } else {
      return false;
    }
  }
}
