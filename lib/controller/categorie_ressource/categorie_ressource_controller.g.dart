// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'categorie_ressource_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CategorieRessourceController on _CategorieRessourceController, Store {
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_CategorieRessourceController.isLoading'))
          .value;

  final _$listCategorieRessourceAtom =
      Atom(name: '_CategorieRessourceController.listCategorieRessource');

  @override
  List<CategorieRessource> get listCategorieRessource {
    _$listCategorieRessourceAtom.reportRead();
    return super.listCategorieRessource;
  }

  @override
  set listCategorieRessource(List<CategorieRessource> value) {
    _$listCategorieRessourceAtom
        .reportWrite(value, super.listCategorieRessource, () {
      super.listCategorieRessource = value;
    });
  }

  final _$idCatRessSelAtom =
      Atom(name: '_CategorieRessourceController.idCatRessSel');

  @override
  String get idCatRessSel {
    _$idCatRessSelAtom.reportRead();
    return super.idCatRessSel;
  }

  @override
  set idCatRessSel(String value) {
    _$idCatRessSelAtom.reportWrite(value, super.idCatRessSel, () {
      super.idCatRessSel = value;
    });
  }

  final _$errorAtom = Atom(name: '_CategorieRessourceController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$messageAtom = Atom(name: '_CategorieRessourceController.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$errorMessageAtom =
      Atom(name: '_CategorieRessourceController.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$listobsFutureAtom =
      Atom(name: '_CategorieRessourceController.listobsFuture');

  @override
  ObservableFuture<dynamic> get listobsFuture {
    _$listobsFutureAtom.reportRead();
    return super.listobsFuture;
  }

  @override
  set listobsFuture(ObservableFuture<dynamic> value) {
    _$listobsFutureAtom.reportWrite(value, super.listobsFuture, () {
      super.listobsFuture = value;
    });
  }

  final _$getListCategorieRessourceAsyncAction =
      AsyncAction('_CategorieRessourceController.getListCategorieRessource');

  @override
  Future<dynamic> getListCategorieRessource() {
    return _$getListCategorieRessourceAsyncAction
        .run(() => super.getListCategorieRessource());
  }

  final _$_CategorieRessourceControllerActionController =
      ActionController(name: '_CategorieRessourceController');

  @override
  void setIdCategorieRessSel(String newId) {
    final _$actionInfo =
        _$_CategorieRessourceControllerActionController.startAction(
            name: '_CategorieRessourceController.setIdCategorieRessSel');
    try {
      return super.setIdCategorieRessSel(newId);
    } finally {
      _$_CategorieRessourceControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
listCategorieRessource: ${listCategorieRessource},
idCatRessSel: ${idCatRessSel},
error: ${error},
message: ${message},
errorMessage: ${errorMessage},
listobsFuture: ${listobsFuture},
isLoading: ${isLoading}
    ''';
  }
}
