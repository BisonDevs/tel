import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/core/core_controller.dart';
import 'page/fil_actualite_non_connecte_page.dart';
import 'page/fil_actualite_connect_page.dart';

class ActualitePage extends StatefulWidget {
  @override
  _ActualitePageState createState() => _ActualitePageState();
}

class _ActualitePageState extends State<ActualitePage> {
  CoreController coreController;

  @override
  void initState() {
    coreController = Provider.of<CoreController>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: coreController.isConnect
          ? FilActualiteConnectPage()
          : FilActualiteNonConnectePage(),
    );
  }
}
