// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ressource_api.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RessourceApi _$RessourceApiFromJson(Map<String, dynamic> json) {
  return RessourceApi(
    titre: json['titre'] as String,
    contenu: json['contenu'] as String,
    typeRessource: json['typeRessource'] as String,
    categorieRessource: json['categorieRessource'] as String,
  );
}

Map<String, dynamic> _$RessourceApiToJson(RessourceApi instance) =>
    <String, dynamic>{
      'titre': instance.titre,
      'contenu': instance.contenu,
      'typeRessource': instance.typeRessource,
      'categorieRessource': instance.categorieRessource,
    };
