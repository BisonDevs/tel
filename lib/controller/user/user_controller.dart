import 'package:image_picker/image_picker.dart';
import 'package:mobx/mobx.dart';
import 'dart:io';

import 'package:ressource_relationnel/data/user/repository_user.dart';
import 'package:ressource_relationnel/modele/catch_error.dart';
import 'package:ressource_relationnel/modele/user/user.dart';

part 'user_controller.g.dart';

class UserController = _UserController with _$UserController;

abstract class _UserController with Store {
  RepositoryUser repositoryUser;
  User currentUser;

  _UserController(this.repositoryUser, {this.currentUser});

  @observable
  String errorMessage;

  @observable
  String message = "";

  @observable
  bool error = false;

  @observable
  ObservableFuture requestUserProfilFuture;

  @observable
  User user;

  @observable
  ObservableFuture requestListUsersFuture;

  @observable
  List<User> users = List<User>();

  @computed
  bool get isLoading {
    return requestUserProfilFuture?.status == FutureStatus.pending;
  }

  @computed
  bool get UserListIsLoading {
    return requestListUsersFuture?.status == FutureStatus.pending;
  }

  @observable
  ImagePicker picker = ImagePicker();

  @observable
  File image;

  @action
  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      image = File(pickedFile.path);
      uploadFile(image);
    } else {
      print('No image selected.');
    }
  }

  @action
  Future uploadFile(File image) async {
    await repositoryUser.uploadImage(image);
  }

  @action
  getUserProfil(User choiceUser) async {
    error = false;
    requestUserProfilFuture =
        ObservableFuture(repositoryUser.getUserProfil(choiceUser, currentUser));
    var requestResult = await CatchError.handle(requestUserProfilFuture);
    if (requestResult.isSuccess()) {
      error = false;
      this.user = requestResult.result;
      return error;
    } else {
      error = true;
      errorMessage = requestResult.errorMessage;
      return error;
    }
  }

  @action
  Future<bool> getListUsers() async {
    error = false;
    requestListUsersFuture = ObservableFuture(repositoryUser.getUserList(currentUser));
    var requestResult = await CatchError.handle(requestListUsersFuture);
    if (requestResult.isSuccess()) {
      error = false;
      this.users = requestResult.result;
      users.forEach((element) {
        print(element.nom);
      });
      return error;
    } else {
      error = true;
      errorMessage = requestResult.errorMessage;
      return error;
    }
  }

  @action
  Future<List<User>> filterListUsers(String text) async {
    print(text);
    error = false;
    requestListUsersFuture = ObservableFuture(repositoryUser.getUserListFilter(currentUser,text));
    var requestResult = await CatchError.handle(requestListUsersFuture);
    if (requestResult.isSuccess()) {
      error = false;
      this.users = requestResult.result;
      users.forEach((element) {
        print(element.nom);
      });
      return users;
    } else {
      error = true;
      errorMessage = requestResult.errorMessage;
      return users;
    }
  }


  @action setCurrentUser(User newUser){
    this.currentUser = newUser;
  }
}
