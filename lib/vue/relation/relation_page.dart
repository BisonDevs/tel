import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/core/core_controller.dart';
import 'package:ressource_relationnel/controller/relation_navigation/relation_navigation_controller.dart';
import 'package:ressource_relationnel/controller/type_relation/type_relation_controller.dart';
import 'package:ressource_relationnel/data/relation/repository_relation.dart';
import 'package:ressource_relationnel/vue/relation/page/relation_disconnect_page.dart';
import 'package:ressource_relationnel/vue/relation/widget/ajouter_relation.dart';
import 'package:ressource_relationnel/main.i18n.dart';

class RelationPage extends StatefulWidget {
  @override
  _RelationPageState createState() => _RelationPageState();
}

class _RelationPageState extends State<RelationPage> {
  CoreController coreController;
  TypeRelationController typeRelationController;
  RelationNavigationController relationNavigationController;

  @override
  void initState() {
    typeRelationController = TypeRelationController(Provider.of<RepositoryRelationAbstract>(context, listen: false));
    coreController = Provider.of<CoreController>(context, listen: false);
    relationNavigationController = RelationNavigationController(coreController.user);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    const double size = 13;
    return coreController.isConnect
        ? DefaultTabController(
            length: 3,
            child: Observer(
              builder: (_) {
                return Scaffold(
                  floatingActionButton: FloatingActionButton(
                    heroTag: "btn2",
                    onPressed: () {
                      return AjouterRelation(context, typeRelationController);
                    },
                    child: Icon(Icons.add),
                  ),
                  body: Column(
                    children: [
                      SizedBox(
                        width: double.infinity,
                        child: CupertinoSegmentedControl(
                          borderColor: Color(0xff44dcc0),
                          selectedColor: Color(0xff44dcc0),
                          groupValue: relationNavigationController.tabItemSelected,
                          padding: EdgeInsets.only(top: 35, left: 8, right: 8),
                          onValueChanged: (int value) {
                            switch (value) {
                              case 0:
                                {
                                  relationNavigationController.changeTabSelected(value);
                                  break;
                                }
                              case 1:
                                {
                                  relationNavigationController.changeTabSelected(value);
                                  break;
                                }
                              case 2:
                                {
                                  relationNavigationController.changeTabSelected(value);
                                  break;
                                }
                              default:
                                {
                                  relationNavigationController.changeTabSelected(0);
                                  break;
                                }
                            }
                          },
                          children: {
                            0: Text(
                              "Mes relations".i18n,
                              style: TextStyle(
                                fontSize: size,
                              ),
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.center,
                              maxLines: 1,
                            ),
                            1: Text(
                              "Mes demandes en attentes".i18n,
                              style: TextStyle(fontSize: size),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              textAlign: TextAlign.center,
                            ),
                            2: Text(
                              "Mes relations en attentes".i18n,
                              style: TextStyle(fontSize: size),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.center,
                            ),
                          },
                        ),
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Container(
                            padding: EdgeInsets.only(top: 10),
                            height: MediaQuery.of(context).size.height *0.70,
                            width: 300,
                            child: relationNavigationController.displayWidget(),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          )
        : RelationDisconnectPage();
  }
}
