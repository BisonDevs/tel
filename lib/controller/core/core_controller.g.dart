// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'core_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CoreController on _CoreController, Store {
  final _$isConnectAtom = Atom(name: '_CoreController.isConnect');

  @override
  bool get isConnect {
    _$isConnectAtom.reportRead();
    return super.isConnect;
  }

  @override
  set isConnect(bool value) {
    _$isConnectAtom.reportWrite(value, super.isConnect, () {
      super.isConnect = value;
    });
  }

  final _$userAtom = Atom(name: '_CoreController.user');

  @override
  User get user {
    _$userAtom.reportRead();
    return super.user;
  }

  @override
  set user(User value) {
    _$userAtom.reportWrite(value, super.user, () {
      super.user = value;
    });
  }

  final _$_CoreControllerActionController =
      ActionController(name: '_CoreController');

  @override
  dynamic setIsConnect(bool newIsConnect) {
    final _$actionInfo = _$_CoreControllerActionController.startAction(
        name: '_CoreController.setIsConnect');
    try {
      return super.setIsConnect(newIsConnect);
    } finally {
      _$_CoreControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setUser(User newUser) {
    final _$actionInfo = _$_CoreControllerActionController.startAction(
        name: '_CoreController.setUser');
    try {
      return super.setUser(newUser);
    } finally {
      _$_CoreControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isConnect: ${isConnect},
user: ${user}
    ''';
  }
}
