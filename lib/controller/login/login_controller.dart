import 'dart:convert';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/user/repository_user.dart';
import 'package:ressource_relationnel/modele/catch_error.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/main.i18n.dart';
import 'package:shared_preferences/shared_preferences.dart';
part 'login_controller.g.dart';

class LoginController = _LoginController with _$LoginController;

abstract class _LoginController with Store {
  RepositoryUser repositoryUser;

  _LoginController(this.repositoryUser){
    getListUserSave();
  }

  @observable
  String userToken;

  @observable
  User user;

  @observable
  String email;

  @observable
  String password;

  @observable
  String msg = "Connexion en cours...".i18n;

  @observable
  String errorMsg = "";

  @observable
  bool error = false;

  @observable
  ObservableFuture authentificationFuture;

  @computed
  bool get authentificationIsLoading {
    return authentificationFuture?.status == FutureStatus.pending;
  }

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  @observable
  ObservableList<User> listUser = ObservableList();

  List<String> formatJson() {
    return listUser
        .map((element) => jsonEncode(element.toJson(element)))
        .toList();
  }

  @action
  Future<void> removeUserSaveAtIndex(index) async {
    final SharedPreferences prefs = await _prefs;
    listUser.removeAt(index);
    prefs.setStringList('listUser', formatJson());
  }

  @action
  Future<bool> saveUser() async {
    var firstTime = verifFirstTime();
    if (firstTime) {
      print("Passe");
      final SharedPreferences preferences = await _prefs;
      listUser.add(User(email: email, password: password));
      preferences.setStringList('listUser', formatJson());
    }
  }

  @action
  bool verifFirstTime() {
    bool firstTime = true;
    listUser.forEach((element) {
      if (element.email == email) {
        firstTime = false;
      }
    });
    return firstTime;
  }

// Récupère la liste des comptes déjà enregistrés
  @action
  Future getListUserSave() async {
    final SharedPreferences prefs = await _prefs;
    if (prefs.getStringList('listUser') == null) {
      listUser = ObservableList();
    } else {
      var list = prefs
          .getStringList('listUser')
          .map((e) => User.fromJson(json.decode(e)))
          .toList();
      listUser = ObservableList.of(list);
    }
  }

  @action
  Future<bool> authentification(FormBuilderState state) async {
    error = false;


    //email = "user1@gmail.com";
    //password = "AAaa&&11";

    email = state.value["email"];
    password = state.value["mdp"];
    email = "bisondevs@gmail.com";
    password = 'm2wWyHy4~571D!\$\$aoit';
    authentificationFuture =
        ObservableFuture(repositoryUser.authentification(email, password));
    var loginResult = await CatchError.handle(authentificationFuture);
    if (loginResult.isSuccess()) {
      error = false;
      user = loginResult.result;
      return error;
    }else{
      error = true;
      errorMsg = loginResult.errorMessage;
      return error;
    }
  }

  @action
  Future<bool> authentification_auto(String email, String password) async {
    error = false;

    //email = "bisondevs@gmail.com";
    //password = 'm2wWyHy4~571D!\$\$aoit';
    //email = "user1@gmail.com";
    //password = "AAaa&&11";

    authentificationFuture =
        ObservableFuture(repositoryUser.authentification(email, password));
    var loginResult = await CatchError.handle(authentificationFuture);
    if (loginResult.isSuccess()) {
      error = false;
      user = loginResult.result;
      return error;
    } else {
      error = true;
      errorMsg = loginResult.errorMessage;
      return error;
    }
  }
}
