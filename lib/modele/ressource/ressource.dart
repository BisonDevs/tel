import 'package:ressource_relationnel/modele/categorie_ressource/categorie_ressource.dart';
import 'package:ressource_relationnel/modele/type_ressource/type_ressource.dart';
import 'package:ressource_relationnel/modele/user/user.dart';

class Ressource {
  String id;
  String slug;
  String titre;
  Map<String, dynamic> contenu;
  User createur;
  CategorieRessource categorieRessource;
  TypeRessource typeRessource;

  Ressource({
    this.id,
    this.slug,
    this.titre,
    this.contenu,
    this.createur,
    this.categorieRessource,
    this.typeRessource,
  });

  factory Ressource.fromJson(Map<String, dynamic> json) {
    return Ressource(
      id: json['@id'] as String,
      slug: json['slug'] as String,
      titre: json['titre'] as String,
      contenu: json['contenu'] as Map<String, dynamic>,
      createur: json['createur'] == null
          ? null
          : User.fromJson(json['createur'] as Map<String, dynamic>),
      categorieRessource: json['categorieRessource'] == null
          ? null
          : CategorieRessource.fromJson(
              json['categorieRessource'] as Map<String, dynamic>),
      typeRessource: json['typeRessource'] == null
          ? null
          : TypeRessource.fromJson(
              json['typeRessource'] as Map<String, dynamic>),
    );
  }

  Map<String, dynamic> toJson(Ressource instance) => <String, dynamic>{
        '@id': instance.id,
        'slug': instance.slug,
        'titre': instance.titre,
        'contenu': instance.contenu,
        'createur': instance.createur,
        'categorieRessource': instance.categorieRessource,
        'typeRessource': instance.typeRessource,
      };
}
