import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/vue/actualite/actualite_page.dart';
import 'package:ressource_relationnel/vue/profil/ma_page_profil.dart';

import 'package:ressource_relationnel/vue/ressources/ressource_home.dart';
import 'package:ressource_relationnel/vue/setting/parametre_page.dart';
import 'package:ressource_relationnel/vue/relation/relation_page.dart';

part 'navigation_controller.g.dart';

class NavigationController = _NavigationController with _$NavigationController;

abstract class _NavigationController with Store{
  @observable
  int tabItemSelected = 0;

  @observable
  List<Widget> listPages = <Widget>[
    MaPageProfil(),
    RessourceHome(),
    ActualitePage(),
    RelationPage(),
    ParametrePage(),
  ];

  @action
  void changeTabSelected(int index) => tabItemSelected = index;
}