class TypeRessource{
    String id;
    String slug;
    String libelle;
    List lsRessources;

    TypeRessource({this.id, this.slug, this.libelle, this.lsRessources});
    factory TypeRessource.fromJson(Map<String,dynamic> json)  {
        return TypeRessource(
            id: json['@id'] as String,
            slug: json['slug'] as String,
            libelle: json['libelle'] as String,
            lsRessources: json['lsRessources'] as List,
        );
    }
    Map<String,dynamic> toJson(TypeRessource instance) =>
    <String, dynamic>{
    '@id': instance.id,
    'slug': instance.slug,
    'libelle': instance.libelle,
    'lsRessources': instance.lsRessources,
    };

}