import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/relation_attente/mes_demandes_controller.dart';
import 'package:ressource_relationnel/data/relation/repository_relation.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/vue/commun/loading_stack.dart';
import 'package:ressource_relationnel/main.i18n.dart';
import 'package:ressource_relationnel/vue/relation/widget/supprimer_dialog.dart';

class MesDemandesRelationsPage extends StatefulWidget {
  User currentUser;

  MesDemandesRelationsPage({this.currentUser});

  @override
  _MesDemandesRelationsPageState createState() =>
      _MesDemandesRelationsPageState();
}

class _MesDemandesRelationsPageState extends State<MesDemandesRelationsPage> {
  MesDemandesController mesDemandesController;
  User currentUser;

  @override
  void initState() {
    mesDemandesController = MesDemandesController(
        Provider.of<RepositoryRelationAbstract>(context, listen: false),
        widget.currentUser);
    currentUser = widget.currentUser;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        return LoadingStack(
          message: mesDemandesController.message,
          visible: mesDemandesController.isLoading,
          body: ListView.builder(
            itemCount: mesDemandesController.listRelation.length,
            itemBuilder: (context, index) {
              var item = mesDemandesController.listRelation[index];
              return mesDemandesController.listRelation.length == 0
                  ? Center(
                      child: Container(
                        child: Text(
                            "Vous n'avez pas encore fait de demande de relation".i18n),
                      ),
                    )
                  : Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.grey[400], width: 0.5),
                        color: Colors.white,
                      ),
                      child: Slidable(
                        actionPane: SlidableDrawerActionPane(),
                        actionExtentRatio: 0.25,
                        child: Container(
                          child: ListTile(
                            leading: CircleAvatar(
                              backgroundColor: Colors.blue,
                              // image de la personne qui recoit l'invitation
                              radius: 20,
                            ),
                            title: AutoSizeText(
                              currentUser.uuid == item.demandeur.uuid ?
                              '${item.receveur.prenom} ${item.receveur.nom}' :
                              '${item.demandeur.prenom} ${item.demandeur.nom}',
                              maxLines: 2,
                            ),
                            subtitle: AutoSizeText(
                              'Demande une relation de type : ${item.typeRelation.libelle}',
                              maxLines: 3,
                            ),
                          ),
                        ),
                        actions: [
                          IconSlideAction(
                            caption: 'Modifier'.i18n,
                            color: Colors.blue,
                            icon: Icons.carpenter,
                            onTap: () {},
                          )
                        ],
                        secondaryActions: <Widget>[
                          IconSlideAction(
                            caption: 'Supprimer'.i18n,
                            color: Colors.red,
                            icon: Icons.delete,
                            onTap: () {
                              SupprimerDialog(
                                  context: context,
                                  controller: mesDemandesController,
                                  relation: item);
                            },
                          ),
                        ],
                      ),
                    );
            },
          ),
        );
      },
    );
  }
}
